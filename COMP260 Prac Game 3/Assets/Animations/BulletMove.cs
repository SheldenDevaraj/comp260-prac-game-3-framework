﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 5.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	public float counter= 10.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>(); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
		counter -= Time.deltaTime;
		if (counter <= 0) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

}
